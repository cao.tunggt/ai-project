import React from "react";

const AuthLayout = (props: { children: React.ReactNode }) => {
  return (
    <div className="flex flex-col items-center justify-center h-screen">
      {props.children}
    </div>
  );
};

export default AuthLayout;
