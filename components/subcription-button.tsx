"use client";

import React, { useState } from "react";
import { Button } from "./ui/button";
import { Sparkle } from "lucide-react";
import { cn } from "@/lib/utils";
import axios from "axios";
import { toast } from "@/components/ui/use-toast";

interface SubscriptionProps {
  className?: string;
  isPro?: boolean;
}

const SubscriptionButton: React.FC<SubscriptionProps> = ({
  className,
  isPro,
}) => {
  const [loading, setLoading] = useState<boolean>(false);
  const handleSubcribe = async () => {
    try {
      setLoading(true);
      const { data } = await axios.get("/api/stripe");
      //todo something after get data from stripe
    } catch (error) {
      toast({
        variant: "destructive",
        description: "Something went wrong. Please try again ⚠",
      });
    } finally {
      setLoading(false);
    }
  };

  return (
    <div className={className}>
      <Button
        variant="outline"
        size="lg"
        disabled={loading}
        onClick={handleSubcribe}
        className={cn(
          "text-white border-none gradient-btn w-full font-semibold",
          "hover:text-white"
        )}
      >
        <span>{isPro ? "Manage Subcription" : "Upgrade to Pro"}</span>
        <Sparkle />
      </Button>
    </div>
  );
};

export default SubscriptionButton;
