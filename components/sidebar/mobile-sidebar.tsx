"use client";

import {
  Sheet,
  SheetContent,
  SheetTrigger,
} from "@/components/ui/sheet";
import { useSidebarStore } from "@/stores/sidebar-store";
import Sidebar from ".";
import { Button } from "../ui/button";

interface MobileSidebarProps {
  isProPlan?: boolean;
  userLimitCount: number;
}

const MobileSidebar: React.FC<MobileSidebarProps> = ({ isProPlan, userLimitCount }) => {
  const { isOpen } = useSidebarStore();

  return (
    <Sheet>
      <SheetContent side="left" className="w-screen border-none bg-black p-0 pt-8">
        <Sidebar 
          isProPlan={isProPlan}
          userLimitCount={userLimitCount} />
      </SheetContent>
    </Sheet>
  )
}

export default MobileSidebar;